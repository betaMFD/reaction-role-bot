import { ReactionRole } from 'discordjs-reaction-role';
import {
  brain as db
} from 'bot-commons-utils/src/utils/brain.mjs';

async function initBrain() {
  await db.read(); // Load the database from file

  // deal with the brainless condition
  if (db.data == null) {
    db.data = {};
  }

  // Make sure the 'guilds' property exists
  if (!db.data.guilds) {
    db.data.guilds = {};
  }
    await db.write(); // Write any changes back to the file
}

// Initialize the database for a specific guild
async function initBrainforGuild(guildId) {
  await initBrain();

  // Make sure this guild's data exists and contains an empty 'reactions' object
  if (!db.data.guilds[guildId]) {
    db.data.guilds[guildId] = { reactions: {} };
    await db.write(); // Write the changes back to the file
  }

  // Write any changes to the database back to the file
  await db.write();
}

async function getReactionConfig() {
  await initBrain();

  // return all reactions
  return Object.values(db.data.guilds)
    .map(guild => guild.reactions)
    .reduce((acc, val) => acc.concat(Object.values(val)), [])
    .flat();
}

async function resetRoleManager(client)
{
  client.roleManager.teardown();
  const react_config = await getReactionConfig();
  const manager = new ReactionRole(client, react_config);
  client.roleManager = manager;
}

// Export the 'initBrain' function and the 'db' object for use in other files
export { initBrainforGuild, initBrain, db, getReactionConfig, resetRoleManager };
