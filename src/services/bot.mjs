import {Client} from 'discord.js';

export class Bot {
  /** @var {Client<true>} */
  client;

}

/** Container for accessing global services. */
export const bot = new Bot();
