import { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, PermissionFlagsBits } from 'discord.js';
import { initBrainforGuild, db, resetRoleManager } from '../utils/brain.mjs';

export const data = new SlashCommandBuilder()
  .setName('add-reaction-role-to-message')
  .setDescription('Saves a reaction-role configuration for a message to the brain')
  .addStringOption(option =>
    option.setName('messageid')
      .setDescription('The message ID')
      .setRequired(true))
  .addStringOption(option =>
    option.setName('reaction')
      .setDescription('The reaction icon')
      .setRequired(true))
  .addStringOption(option =>
    option.setName('roleid')
      .setDescription('The role ID')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.ManageRoles);

export async function execute(interaction, client) {
  const guildId = interaction.guild.id;
  const messageId = interaction.options.getString('messageid');
  const reaction = interaction.options.getString('reaction');
  const roleId = interaction.options.getString('roleid');

  await initBrainforGuild(guildId);

  const guild = db.data.guilds[guildId];
  const message = guild.reactions[messageId] || [];

  // Check if reaction already exists for this message ID
  const existingReaction = message.find(r => r.reaction === reaction);
  if (existingReaction) {
    const row = new ActionRowBuilder()
      .addComponents(
        new ButtonBuilder()
          .setCustomId('replace-reaction-role')
          .setLabel('Replace Reaction Role')
          .setStyle('Primary')
          .setEmoji('🔁')
          .setDisabled(false),
        new ButtonBuilder()
          .setCustomId('cancel-replace-reaction-role')
          .setLabel('Cancel')
          .setStyle('Secondary')
          .setEmoji('🚫')
          .setDisabled(false)
      );

    await interaction.reply({
      content: `A reaction role configuration for reaction ${reaction} and message ${messageId} already exists with role ${existingReaction.roleId}.`,
      components: [row],
      ephemeral: true
    });

    const filter = (i) => ['replace-reaction-role', 'cancel-replace-reaction-role'].includes(i.customId) && i.user.id === interaction.user.id;
    const collector = interaction.channel.createMessageComponentCollector({ filter, time: 15000 });

    collector.on('collect', async (i) => {
      if (i.customId === 'replace-reaction-role') {
        existingReaction.roleId = roleId;
        await db.write();
        await resetRoleManager(client);
        await interaction.editReply({
          content: `Reaction role configuration for reaction ${reaction} and message ${messageId} has been updated to role ${roleId}.`,
          components: []
        });
      } else {
        await interaction.editReply({
          content: 'Cancelled.',
          components: []
        });
      }
    });

    collector.on('end', async (collected) => {
      if (collected.size === 0) {
        await interaction.editReply({
          content: 'Timed out.',
          components: []
        });
      }
    });  } else {
    try {
      const messageObj = await interaction.channel.messages.fetch(messageId);
      if (messageObj) {
        const roleObj = await interaction.guild.roles.fetch(roleId);
        if (!roleObj) {
          await interaction.reply({
            content: `Could not find a role with ID ${roleId}.`,
            ephemeral: true,
          });
        } else {
          await messageObj.react(reaction);
          message.push({ messageId, reaction, roleId });
          guild.reactions[messageId] = message;
          await db.write();
          await resetRoleManager(client);
          await interaction.reply({
            content: `Reaction role configuration for message ${messageId} and reaction ${reaction} saved.`,
            ephemeral: true,
          });
        }
      } else {
        await interaction.reply({
          content: `Could not find a message with ID ${messageId}.`,
          ephemeral: true,
        });
      }
    } catch (error) {
      console.error(`Error reacting to message ${messageId}:`, error);
      await interaction.reply({
        content: `Could not react to message with ID ${messageId}. Please make sure the message exists and that bot has the necessary permissions and try again.`,
        ephemeral: true,
      });
    }
  }
}
