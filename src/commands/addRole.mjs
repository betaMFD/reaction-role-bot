import { SlashCommandBuilder, PermissionFlagsBits } from 'discord.js';

export const data = new SlashCommandBuilder()
  .setName('add-role')
  .setDescription('Creates a new role on the server, without needing to go into the settings.')
  .addStringOption(option =>
    option.setName('role-name')
      .setDescription('Role Name')
      .setRequired(true))
  .setDefaultPermission(false)
  .setDefaultMemberPermissions(PermissionFlagsBits.ManageRoles);

export async function execute(interaction, client) {
  const roleName = interaction.options.getString('role-name');
  const guild = interaction.guild;

  // Check if the user has permission to run this command
  const hasPermission = interaction.member.permissions.has('ADMINISTRATOR');
  if (!hasPermission) {
    return interaction.reply({
      content: 'You do not have permission to use this command.',
      ephemeral: true,
    });
  }

  try {
    const newRole = await guild.roles.create({
      name: roleName,
    });

    // Remove perms from @everyone to view the channel, message in it, or create threads
    await newRole.setPermissions ([]);

    await interaction.reply({
      content: `Role **${newRole.name}** created with ID **${newRole.id}**`,
      ephemeral: true,
    });
  } catch (error) {
    console.error(`Error creating role ${roleName}:`, error);
    await interaction.reply({
      content: `Could not create role ${roleName}. Please make sure the bot has the necessary permissions and try again.`,
      ephemeral: true,
    });
  }
}
