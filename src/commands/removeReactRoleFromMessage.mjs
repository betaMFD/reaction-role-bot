import { SlashCommandBuilder, ActionRowBuilder, ButtonBuilder, PermissionFlagsBits } from 'discord.js';
import { initBrainforGuild, db, resetRoleManager } from '../utils/brain.mjs';

async function handleButtonClick(interaction, messageId, selectedReaction, client) {
  const guildId = interaction.guildId;

  await initBrainforGuild(guildId);

  // Check if the user clicked the cancel button
  if (selectedReaction === 'cancel') {
    return await interaction.update({
      content: `You have cancelled removing a reaction role from message ${messageId}.`,
      components: []
    });
  }

  // make sure THIS guild exists and has reactions inside
  if (!db.data.guilds[guildId].reactions) {
    return await interaction.reply({
      content: 'There are no reaction roles configured for this guild.',
      ephemeral: true
    });
  }

  // check if message has reaction role configuration
  if (!db.data.guilds[guildId].reactions[messageId]) {
    return await interaction.reply({
      content: `There are no reaction roles configured for message ${messageId}.`,
      ephemeral: true
    });
  }

  const guild = db.data.guilds[guildId];
  const message = guild.reactions[messageId];

  // Check if selectedReaction exists for this message ID
  const existingReactionIndex = message.findIndex(r => r.reaction === selectedReaction);
  if (existingReactionIndex === -1) {
    return await interaction.reply({
      content: `There is no reaction role configured for reaction ${selectedReaction} and message ${messageId}.`,
      ephemeral: true
    });
  }

  // Remove the reaction role from the message
  message.splice(existingReactionIndex, 1);
  if (message.length === 0) {
    delete guild.reactions[messageId];
  }

  await db.write();
  await resetRoleManager(client);
  const messageObj = await interaction.channel.messages.fetch(messageId);
  if (messageObj) {
    await messageObj.reactions.cache.get(selectedReaction).remove();
  }

  // offer a new set of buttons to select from
  const newButtons = await getButtons(messageId, message.map(r => r.reaction));
  await interaction.update({
    content: `Reaction role configuration for reaction ${selectedReaction} and message ${messageId} has been removed. Select another reaction role to remove:`,
    components: newButtons
  });
}

async function getButtons(messageId, reactions) {
  const buttons = [];
  for (let i = 0; i < reactions.length; i++) {
    buttons.push(new ButtonBuilder()
      .setCustomId(`removeReactRole_${messageId}_${reactions[i]}`)
      .setLabel(reactions[i])
      .setStyle('Primary'));
  }
  buttons.push(new ButtonBuilder()
    .setCustomId(`removeReactRole_${messageId}_cancel`)
    .setLabel('Cancel')
    .setStyle('Secondary'));

  const chunks = chunkArray(buttons, 5);
  const buttonRows = [];
  for (const chunk of chunks) {
    buttonRows.push(new ActionRowBuilder().addComponents(chunk));
  }
  return buttonRows;
}

function chunkArray(array, size) {
  const chunks = [];
  for (let i = 0; i < array.length; i += size) {
    chunks.push(array.slice(i, i + size));
  }
  return chunks;
}

export const data = new SlashCommandBuilder()
  .setName('remove-react-role-from-message')
  .setDescription('Remove a reaction role from a message.')
  .addStringOption(option =>
    option.setName('messageid')
      .setDescription('The ID of the message.')
      .setRequired(true))
  .setDefaultMemberPermissions(PermissionFlagsBits.ManageRoles);

export async function execute(interaction, client) {
  const messageId = interaction.options.getString('messageid');
  const guildId = interaction.guildId;

  await initBrainforGuild(guildId);

  // make sure THIS guild exists and has reactions inside
  if (!db.data.guilds[guildId].reactions) {
    return await interaction.reply({
      content: 'There are no reaction roles configured for this guild.',
      ephemeral: true
    });
  }

  const guild = db.data.guilds[guildId];

  // check if message has reaction role configuration
  if (!guild.reactions[messageId]) {
    return await interaction.reply({
      content: `There are no reaction roles configured for message ${messageId}.`,
      ephemeral: true
    });
  }

  const message = guild.reactions[messageId];

  // Check if there are any reaction roles for this message ID
  if (message.length === 0) {
    return await interaction.reply({
      content: `There are no reaction roles configured for message ${messageId}.`,
      ephemeral: true
    });
  }

  let content = `Which reaction role do you want to remove from message ${messageId}?\n`;

  const components = [];

  while (message.length > 0) {
    const row = new ActionRowBuilder();
    for (let i = 0; i < Math.min(message.length, 5); i++) {
      const reactionRole = message[i];
      const button = new ButtonBuilder()
        .setCustomId(`removeReactRole_${messageId}_${reactionRole.reaction}`)
        .setLabel(reactionRole.reaction)
        .setStyle('Primary');
      row.addComponents(button);
    }
    components.push(row);
    message.splice(0, 5);
  }

  components.push(new ActionRowBuilder().addComponents(
    new ButtonBuilder()
      .setCustomId(`removeReactRole_${messageId}_cancel`)
      .setLabel('Cancel')
      .setEmoji('🚫')
      .setStyle('Secondary')
  ));

  await interaction.reply({
    content: content,
    components: components,
    ephemeral: true
  });
}


export { handleButtonClick };
