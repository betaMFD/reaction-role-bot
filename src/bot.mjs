import {bot} from './services/bot.mjs';
import { config } from 'dotenv';
import { getReactionConfig } from './utils/brain.mjs';
import Discord, { Client, GatewayIntentBits, Partials } from 'discord.js';
import { ReactionRole } from 'discordjs-reaction-role';
import {registerCommands} from 'bot-commons-utils/src/utils/botSetup/registerCommands.mjs';
import {setupGracefulShutdown} from 'bot-commons-utils/src/utils/botSetup/setupGracefulShutdown.mjs';

config(); //for dotenv

import { handleButtonClick } from './commands/removeReactRoleFromMessage.mjs';
import path from 'node:path';

// Create a new bot.client instance
bot.client = new Client({
  partials: [Partials.Message, Partials.Channel, Partials.Reaction],
  intents:[GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.GuildMessageReactions],
  });

const react_config = await getReactionConfig();
const manager = new ReactionRole(bot.client, react_config);
bot.client.roleManager = manager;


// Define the bot's root directory
const botRoot = path.resolve('.');
// Register all commands in the folder
await registerCommands(bot, botRoot);


// Start the bot.
bot.client.on("ready", () => {
  console.log("Bot is online! DJS version:", Discord.version);
});

// Login to Discord with your bot.client's token
bot.client.login(process.env.DISCORD_TOKEN);


bot.client.on('interactionCreate', async interaction => {
  if (interaction.isButton()) {
    const [action, messageId, selectedReaction] = interaction.customId.split('_');

    if (action === 'removeReactRole') {
      await handleButtonClick(interaction, messageId, selectedReaction, bot.client);
    }
  }

  if (!interaction.isCommand()) return;

  const command = bot.client.commands.get(interaction.commandName);
  if (!command) return;

  try {
    await command.execute(interaction, bot.client);
  } catch (error) {
    console.error(error);
    await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
  }
});


// Stop the bot when the process is closed (via Ctrl-C).
setupGracefulShutdown(bot);
