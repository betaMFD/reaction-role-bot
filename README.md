# Discord Role Bot

Uses Discord.js.

## Required Bot Permissions

Invite scopes:
 * bot
 * applications.commands

Bot Permissions:
 * Manage Roles
 * Read Messages / View Channels
 * Read Message History
 * Add Reactions

Permission integer: 268502080


Slide your bot up to the top of the permission stack of any Roles you want it to apply, so it can apply 
the roles below it. 

Make sure the bot can see the channel(s) you want to use it in!

I've had to turn on "add reactions" exclusively in the channels I wanted the role bot to work in, although it had it as a global permission so I'm not sure why.
